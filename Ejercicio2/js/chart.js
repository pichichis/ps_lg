$(function () {
    
   // Build the chart
   var optionsPie = {
        chart: {
            renderTo: 'pie-container',
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Test'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                }
            }
        },
       series: [{}]
    };

    
   var optionsLine = {
        chart: {
            renderTo: 'line-container',
        },
        title: {
            text: 'Test'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150,
            maxZoom: 20 * 1000
        },
        yAxis: {
            minPadding: 0.2,
            maxPadding: 0.2,
            title: {
                text: 'Value',
                margin: 80
            }
        },
       legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{ data: [] },{data: []},{data: []}]
    };      
    
    
    getDataLine(optionsLine).then(function (optionsLine) {  
         var chart = new Highcharts.Chart(optionsLine);
    })
  
    getDataPie(optionsPie).then(function (optionsPie) {  
         var chart = new Highcharts.Chart(optionsPie);
    })
    
});