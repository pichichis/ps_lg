/* función que recibe los datos en crudo y va iterando, buscando propiedades características de cada flujo de datos,
 * construye un array asociativo por fechas, para facilitarnos la acumulación de datos, en caso de que sean de la misma fecha.
 * una vez finalizadas todas las tramas, devuelven un objeto, preparado para la ingesta de datos en el chart y la suma total de categorías para  * el  pie */

function parser(data){
    
    var categoria1 = {};
    var categoria2 = {};
    var categoria3 = {};
    
    data.forEach(function(current_value,index){
        
        if( current_value.hasOwnProperty('cat') ) {
            
            switch(current_value.cat) {
                case "Cat 1":
                    if (categoria1.hasOwnProperty(current_value.d)){
                        
                        categoria1[current_value.d] = categoria1[current_value.d] + current_value.value 
                    }else{
                        categoria1[current_value.d] = current_value.value
                    }
                    break;
                case "Cat 2":
                    if (categoria2.hasOwnProperty(current_value.d)){
                        
                        categoria2[current_value.d] = categoria2[current_value.d] + current_value.value 
                    }else{
                        categoria2[current_value.d] = current_value.value
                    }
                    break;
                default:
                    if (categoria3.hasOwnProperty(current_value.d)){
                        
                        categoria3[current_value.d] = categoria3[current_value.d] + current_value.value 
                    }else{
                        categoria3[current_value.d] = current_value.value
                    }
            }//switch
            
        }//if cat
        
        if( current_value.hasOwnProperty('categ') ) {
            
            date = new Date(current_value.myDate);
            current_value.myDate = date.getTime();
            
            switch(current_value.categ) {
                case "CAT 1":
                    if (categoria1.hasOwnProperty(current_value.myDate)){
                        
                        categoria1[current_value.myDate] = categoria1[current_value.myDate] + current_value.val 
                    }else{
                        categoria1[current_value.myDate] = current_value.val
                    }
                    break;
                case "CAT 2":
                    if (categoria2.hasOwnProperty(current_value.myDate)){
                        
                        categoria2[current_value.myDate] = categoria2[current_value.myDate] + current_value.val 
                    }else{
                        categoria2[current_value.myDate] = current_value.val
                    }
                    break;
                default:
                    if (categoria3.hasOwnProperty(current_value.myDate)){
                        
                        categoria3[current_value.myDate] = categoria3[current_value.myDate] + current_value.val 
                    }else{
                        categoria3[current_value.myDate] = current_value.val
                    }
            } //switch
            
        }//if categ
        
        if( current_value.hasOwnProperty('raw') ) {
             
             categoria = current_value.raw.match(/cat\s\d/)[0];
             fecha = current_value.raw.match(/\d\d\d\d-\d\d-\d\d/)[0]
             date = new Date(fecha);
             fecha_normalizada = date.getTime();

             switch(categoria) {
                case "cat 1":
                    if (categoria1.hasOwnProperty(fecha_normalizada)){
                        
                        categoria1[fecha_normalizada] = categoria1[fecha_normalizada] + current_value.val 
                    }else{
                        categoria1[fecha_normalizada] = current_value.val
                    }
                    break;
                case "cat 2":
                    if (categoria2.hasOwnProperty(fecha_normalizada)){
                        
                        categoria2[fecha_normalizada] = categoria2[fecha_normalizada] + current_value.val 
                    }else{
                        categoria2[fecha_normalizada] = current_value.val
                    }
                    break;
                default:
                    if (categoria3.hasOwnProperty(fecha_normalizada)){
                        
                        categoria3[fecha_normalizada] = categoria3[fecha_normalizada] + current_value.val 
                    }else{
                        categoria3[fecha_normalizada] = current_value.val
                    }
              } //switch
             
         }//if raw
         
    });
    
    var cat1 =contruirSerie(categoria1)
    var totalCat1 = sumarValores(cat1)
    var cat2 =contruirSerie(categoria2)
    var totalCat2 = sumarValores(cat2)
    var cat3 =contruirSerie(categoria3)
    var totalCat3 = sumarValores(cat3)

    
    datos_parseados = {cat1,cat2,cat3,totalCat1,totalCat2,totalCat3};
    return datos_parseados;
}

//convertimos el objeto en un array [ valor , fecha ] para la ingesta del chart
function contruirSerie(categoria){
    var cat = []
    
    for (var propiedad in categoria) {
        cat.push([parseFloat(propiedad),parseFloat(categoria[propiedad])]) 
    }
    
    return cat;
}

//sumamos por cada categoría los valores para mostraslos en el pie
function sumarValores(cat){
    var total = 0.0
    
    cat.forEach(function(current_value,index){
        total = total + parseFloat(current_value[1]);
    });
    
    return total;
}

/* función principal retorna una promesa por tener una llamada asíncrona, no he podido acceder los 3 documentos de google drive, error 404 
 * asi que me he creado un Json con los 3 tipos de datos, básicamente la función recibe los datos, los parsea, y actualiza las opciones 
 * del chart */

function getDataLine(options){
    return new Promise(function(resolve, reject) {
         
         $.getJSON('./mockdata/dataLog.json', function(data) {
             array_datos_tratados = parser(data);
             cat1 = array_datos_tratados.cat1;
             cat2 = array_datos_tratados.cat2;
             cat3 = array_datos_tratados.cat3;

             options.series[0].data = cat1;
             options.series[0].name = "CAT 1";
             options.series[1].data = cat2;
             options.series[1].name = "CAT 2";
             options.series[2].data = cat3;
             options.series[2].name = "CAT 3";
             
             resolve (options)
         });
    });
}

function getDataPie(options){
    return new Promise(function(resolve, reject) {
         
         $.getJSON('./mockdata/dataLog.json', function(data) {
             array_datos_tratados = parser(data);
             cat1 = array_datos_tratados.totalCat1;
             cat2 = array_datos_tratados.totalCat2;
             cat3 = array_datos_tratados.totalCat3;

             options.series[0].data = [["CAT 1",cat1],["CAT 2",cat2],["CAT 3", cat3]]

             resolve (options)
         });
    });
}