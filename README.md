Ejercicio 1

He optado por una solución basada en la descomposición factorial,
y la búsqueda de sus divisores propios

la función factorial devuelve un diccionario de potencias 
por ej para el valor 12 devolvería {0: [2.0, 4.0], 1: [3.0]} es decir 2^1,2^2,3^1

la función divisores_comunes devuelve una lista con todos los divisores comunes
básicamente coge los valores del array y va múltiplicando las potencias(i) con las potencias(i+1)
además de insertar el propio valor 
para {0: [2.0, 4.0], 1: [3.0]} devolvería [2.0, 4.0,3.0,6.0,12.0]

la función perfecto_abundante_defectivo suma los valores de la lista y devuelve 
si es defectivo, abundante o perfecto
para [2.0, 4.0,3.0,6.0,12.0] devuelve 27.0 > 2 * 12.0 por lo tanto es abundante
sino queremos incluir el último divisor es equivalente a 15.0 > 12.0
 
 
Por último, he comentado una versión más sencilla de hacer pero muy costosa, ya que para números enormes itera
un número de veces igual al número, la he escrito porque no sabía si queréis eficiencia o sencillez

probado en python 3.5.7 y 2.7.12

Ejercicio 2

Index.html contiene los containers del line chart y el pie chart.

links a css de estilos que en este caso no importa mucho y los ficheros javascripts.
En la carpeta mockdata hay un js con los datos de prueba,
los ficheros en googledrive no estaban disponibles, asi que he montado yo uno usando los 3 tipos de datos.

en la carpeta js, tenemos las librerías de highchart,
el fichero chart.js, con la configuración de los 2 charts, la llamada al servicio que pedirá los datos y los parseará.
parserService.js, tiene toda la lógica de ingestión de datos y manipulación, para devolver los data.series que necesita
el objeto options, para mostrar las gráficas.