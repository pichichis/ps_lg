import math

def factorial(numero):
    divisor = 2
    exponente = 1
    factores = []
    potencias = {}
    i = 0

    while (numero >= divisor):
     
      if (numero % divisor == 0):
           numero = numero / divisor
           factores.append (math.pow(divisor,exponente))
           exponente += 1
           potencias[i] = factores
      
      else:
        divisor +=  1
        exponente = 1
        factores = []
        i += 1
    
    return potencias    


def divisores_comunes(potencias):
    solucion = [1.0]
    solucionAux = []
    sol = 0

    for lista_valor in potencias.values():
    
       for valor_solucion in solucion:
           
          for valor in lista_valor:
              x = valor_solucion * valor
              if x not in solucion:
                 solucionAux.append (x)

       solucion += solucionAux
       solucionAux = []

    return solucion
    
def perfecto_abundante_defectivo(numero, sol):
    suma = 0
    for valor in sol:
        suma += valor

    if (numero * 2) == suma:
        print ("el ",numero,"es perfecto")
    elif (numero * 2) > suma:
        print ("el ",numero,"es defectivo")
    else:
        print ("el ",numero,"es abundante")    
        


#Comienza el programa principal
lista = [6,12,14,18,20,27,28,30,71,88,90,96,100,101,102,496,8128,33550336, 111234534]

for numero in lista:

   potencias = factorial (numero)
   sol = divisores_comunes(potencias)
   perfecto_abundante_defectivo(numero,sol)





""" 2 forma nada eficiente

suma = 0

for numero in lista:
    for divisor in range (1, numero):
         if (numero % divisor == 0):
             suma += divisor
             
    if numero == suma:
        print ("el ",numero,"es perfecto")
    elif numero  > suma:
        print ("el ",numero,"es defectivo")
    else:
        print ("el ",numero,"es abundante")

    suma = 0
"""

